﻿namespace DuolingoParser.Processing {

    /// <summary> All information about the student. </summary>
    class DuolingoStudent
    {
        /// <summary> The UČO of a student. </summary>
        public int Uco { get; }

        /// <summary> The lectures completed by the student in Duolingo. </summary>
        public int Lectures { get; }
        
        /// <summary> Did the student complete the placement test? </summary>
        public bool Placement { get; }

        /// <summary> Is the UČO of the student valid? </summary>
        public bool IsValid { get; }

        /// <summary> All the fields in the export. </summary>
        public string[] Fields { get; }

        public DuolingoStudent(int uco, int lectures, bool placement, string[] fields)
        {
            Uco = uco;
            Lectures = lectures;
            Placement = placement;
            IsValid = uco != -1;
            Fields = fields;
        }

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return Fields[0].Trim();
        }
    }
}