﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace DuolingoParser.Processing
{
    static class DuolingoCsv
    {
        /// <summary> Parses a CSV file with Duolingo export and returns the result. </summary>
        /// <param name="path"> A path to a valid CSV file. </param>
        /// <param name="nameColumn"> The numbers of the columns containing name. </param>
        /// <param name="lectureColumn"> The numbers of the columns containing lectures. </param>
        /// <param name="placementColumn"> A column containing the placement test. </param>
        /// <param name="delimiter"> A column separator of the CSV file. </param>
        /// <exception cref="ArgumentNullException"> Thrown when the file path is empty. </exception>
        /// <exception cref="ArgumentException"> Thrown when the file path doesn't exist, or doesn't lead to a CSV file. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the user entered incorrect column identifiers. </exception>
        public static IEnumerable<DuolingoStudent> ReadDuoCsv(string path, string nameColumn,
            string lectureColumn, string placementColumn = "18", string delimiter = ",")
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullException(nameof(path), "Je třeba vyplnit cestu k souboru.");
            }

            if (!System.IO.File.Exists(path) || !path.EndsWith(".csv", StringComparison.InvariantCultureIgnoreCase))
            {
                throw new ArgumentException("Zadaná cesta buď neexistuje, nebo nevede k CSV souboru.", nameof(path));
            }

            var students = new List<DuolingoStudent>();
            using var csvParser = new TextFieldParser(path);

            csvParser.SetDelimiters(delimiter);
            csvParser.HasFieldsEnclosedInQuotes = false;

            //! The first row with column names
            int headersCount = csvParser.ReadFields().Length;

            if (!(TryParseTextToIntArray(nameColumn, out var names) &&
                  TryParseTextToIntArray(lectureColumn, out var activities)))
            {
                throw new InvalidOperationException(
                    "Zadané označení pro sloupce se jménem nebo aktivitami není platné číslo.");
            }
            
            var ucoInName = new Regex(@"\d{4,}", RegexOptions.Compiled);

            while (!csvParser.EndOfData)
            {
                string[] student = csvParser.ReadFields();
                while (student.Length > headersCount)
                {
                    // The name (and only name) may have commas inside
                    string nameWithAComma = student[0] + " " + student[1];
                    var newStudent = new List<string> { nameWithAComma };
                    newStudent.AddRange(student.Skip(2));
                    student = newStudent.ToArray();
                }

                string ucoPart = names.Select(n => student[n])
                    .Aggregate((result, name) => result + ',' + name);
                int uco = NameToUco(ucoPart, ucoInName); // name
                
                int lectures = activities.Select(n => int.Parse(student[n]))
                    .Aggregate((sum, activity) => sum + activity);

                int.TryParse(placementColumn, out int placementIndex);
                int.TryParse(student[Math.Clamp(placementIndex - 1, 0, headersCount - 1)], out int placementValue);

                students.Add(new DuolingoStudent(uco, lectures, placementValue > 0, student));
            }

            var invalidStudents = students.Where(s => !s.IsValid).ToList();
            // Conflate duplicate students based on valid UCO
            var mergedStudents = students.Except(invalidStudents).GroupBy(student => student.Uco)
                .Select(group => new DuolingoStudent(
                    uco:group.Key,
                    group.Sum(s => s.Lectures),
                    group.Any(s => s.Placement),
                    group.OrderByDescending(s => s.Lectures).Select(s => s.Fields).First()))
                .ToList();

            return mergedStudents.Union(invalidStudents);
        }

        /// <summary> Attempts to convert comma-separated numbers into an int array. </summary>
        /// <returns> True if the conversion succeeded. </returns>
        static bool TryParseTextToIntArray(string text, out List<int> integers)
        {
            var numberSequenceRegex = new Regex(@"^\d+(,[ ]*\d+)*$");
            integers = new List<int>();
            if (string.IsNullOrWhiteSpace(text) || !numberSequenceRegex.IsMatch(text))
            {
                return false;
            }

            string[] strings = text.Split(',');
            foreach (string s in strings)
            {
                if (!int.TryParse(s.Trim(), out int number)) continue;
                integers.Add(Math.Max(0, number - 1)); // zero-based columns
            }

            return integers.Any();
        }

        /// <summary> Extracts UČO from the end of the student's name. Returns -1 if not found. </summary>
        /// <param name="name"> The name of the student in Duolingo. </param>
        /// <param name="regex"> The regular expression to use to find people's UČO in their name. </param>
        static int NameToUco(string name, Regex regex)
        {
            name = name.Trim();
            if (string.IsNullOrWhiteSpace(name)) return -1;
            
            Match nameMatch = regex.Match(name);
            if (nameMatch.Success && int.TryParse(nameMatch.Value, out int uco))
            {
                return uco;
            }
            return -1;
        }
    }
}