using System;
using System.IO;
using System.Linq;
using System.Text;

namespace DuolingoParser.Processing
{
    public class TextFieldParser : StreamReader
    {
        int iToken = 1;
        bool quoted = false;
        char[] delimiters;
        string curLine;

        public TextFieldParser(string path) : base(path) { }

        public TextFieldParser(Stream stream) : base(stream) { }

        public string[] ReadFields()
        {
            curLine = ReadLine();
            return GetFields();
        }

        public void SetDelimiters(string delim)
        {
            delimiters = delim.ToCharArray();
        }

        public string[] GetFields()
        {
            if (delimiters == null || delimiters.Length == 0)
            {
                throw new Exception($"{GetType().Name} requires delimiters be defined to identify fields.");
            }

            if (!HasFieldsEnclosedInQuotes)
            {
                return curLine.Split(delimiters);
            }

            char token = (char) iToken;
            var sb = new StringBuilder();

            // Go through the string and change delimiters to token
            // ignoring them if within quotes if indicated
            foreach (char c in curLine)
            {
                char qc = c;

                if (HasFieldsEnclosedInQuotes && qc == '"')
                {
                    quoted = !quoted;
                    continue;
                }
                else if (!quoted)
                {
                    // Replace the delimiters with token
                    if (delimiters.Any(t => qc == t))
                    {
                        qc = token;
                    }
                }

                sb.Append(qc);
            }

            return sb.ToString().Split(token);
        }

        public bool HasFieldsEnclosedInQuotes { get; set; } = false;

        public bool EndOfData
        {
            get { return EndOfStream; }
        }
    }
}
