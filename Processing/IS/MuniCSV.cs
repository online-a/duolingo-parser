﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace DuolingoParser.Processing.IS
{
    static class MuniCsv
    {
        const string CourseCode = "Online_A";
        const char Delimiter = ':';
        const string Extension = "txt";

        /// <summary> Writes the duolingo export into a file. </summary>
        public static void WriteMuniCsv(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            var wa = (DuoHome.OutputData) doWorkEventArgs.Argument;

            if (string.IsNullOrWhiteSpace(wa.folderPath))
            {
                throw new ArgumentNullException(nameof(wa.folderPath), "Je třeba vyplnit cestu k výstupní složce.");
            }
            if (!Directory.Exists(wa.folderPath))
            {
                throw new ArgumentException("Zadaná cesta buď neexistuje, nebo nevede ke složce.", nameof(wa.folderPath));
            }
            
            var sb = new StringBuilder();
            var sbInvalid = new StringBuilder();
            var sbPlacement = new StringBuilder();
            // 481007/ONLINE_A/710073:*10

            if (wa.sorted)
            {
                wa.students.Sort((a,b) => a.Uco.CompareTo(b.Uco));
            }

            for (int i = 0; i < wa.students.Count; i++)
            {
                (sender as BackgroundWorker)?.ReportProgress(Convert.ToInt32(Math.Clamp(i+1 * 100f / wa.students.Count, 0, 100)));
                
                DuolingoStudent student = wa.students[i];
                int points = Math.Clamp(student.Lectures * wa.pointsPerLecture, 0, wa.pointsLimit);
                if (student.IsValid)
                {
                    sb.AppendLine($"{student.Uco}/{CourseCode}/{wa.ropot}{Delimiter}*{points}");
                    if (student.Placement) sbPlacement.AppendLine($"{student.Uco}/{CourseCode}/PLACEMENT{Delimiter}*30");
                }
                else
                {
                    // TODO: Do something with invalid students
                    sbInvalid.AppendLine($"*{points}\t\t{student.Fields[0]} | {student.Fields[1]} | {student.Fields[2]}");
                }
            }
            string outputFilePath = Path.Combine(wa.folderPath, $"ONLINE_A_Duolingo_Week_X.{Extension}");
            string invalidFilePath = Path.Combine(wa.folderPath, $"ONLINE_A_Duolingo_Invalid.{Extension}");
            string placementFilePath = Path.Combine(wa.folderPath, $"ONLINE_A_Duolingo_PlacementTest.{Extension}");
            File.WriteAllText(outputFilePath, sb.ToString()); 
            if (sbInvalid.Length != 0) File.WriteAllText(invalidFilePath, sbInvalid.ToString());
            if (sbPlacement.Length != 0) File.WriteAllText(placementFilePath, sbPlacement.ToString());
            (sender as BackgroundWorker)?.ReportProgress(100);

            // Open folder
            Process.Start("explorer.exe", wa.folderPath);
        }
    }
}
