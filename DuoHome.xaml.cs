﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using DuolingoParser.Processing;
using DuolingoParser.Processing.IS;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

namespace DuolingoParser
{
    /// <summary>
    /// Interaction logic for DuoHome.xaml
    /// </summary>
    public partial class DuoHome : Page
    {
        BackgroundWorker worker;

        // TODO: Hide console app

        //? Publishing single file
        //! dotnet publish -r win10-x64 -p:PublishSingleFile=true -c Release

        public DuoHome()
        {
            InitializeComponent();
            InitializeBackgroundWorker();
        }

        void InitializeBackgroundWorker()
        {
            worker = new BackgroundWorker {WorkerReportsProgress = true};
            worker.DoWork += MuniCsv.WriteMuniCsv;
            worker.ProgressChanged += Worker_ProgressChanged;
        }

        /// <summary> Validates if the text box contains a valid decimal number to convert to points. </summary>
        void PointValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            var pointsRegex = new Regex(@"^[0-9]*\.?[0-9]+$");
            e.Handled = pointsRegex.IsMatch(e.Text);
        }

        /// <summary> Validates if the text box contains a whole number. </summary>
        void WholeNumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            var wholeNumberRegex = new Regex(@"[^0-9]+");
            e.Handled = wholeNumberRegex.IsMatch(e.Text);
        }
        
        /// <summary> Validates if the text box contains a sequence of whole numbers. </summary>
        void WholeNumberSequenceValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            var numberSequenceRegex = new Regex(@"^\d+(,[ ]*\d+)*$");
            e.Handled = numberSequenceRegex.IsMatch(e.Text);
        }

        /// <summary> Find a csv file. </summary>
        void InputFileBrowseButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                    DefaultExt = ".csv",
                    Filter = "Duolingo výstup ‘Spreadsheet’ (*.csv)|*.csv|Všechny soubory (*.*)|*.*",
                    FilterIndex = 0
            };

            // Display the dialog
            if (ofd.ShowDialog() == true)
            {
                InputFileTextBox.Text = ofd.FileName;
                if (string.IsNullOrWhiteSpace(OutputFolderTextBox.Text))
                {
                    OutputFolderTextBox.Tag = Path.GetDirectoryName(ofd.FileName) ?? string.Empty;
                }
            }
        }

        void OutputFolderBrowseButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            using var fbd = new FolderBrowserDialog
            {
                    ShowNewFolderButton = true,
                    UseDescriptionForTitle = true,
                    Description = "Výstupní složka",
            };

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                OutputFolderTextBox.Text = fbd.SelectedPath;
            }
        }

        /// <summary> Starting the conversion. </summary>
        void ConversionButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            List<DuolingoStudent> duoStudents = DuolingoCsv.ReadDuoCsv(InputFileTextBox.Text,
                                                                       NameColumnTextBox.Text,
                                                                       LectureColumnTextBox.Text,
                                                                       PlacementTestColumnTextBox.Text).ToList();
            // TODO: Why was it commented out?
            // ConversionProgressBar.Minimum = 0;
            // ConversionProgressBar.Maximum = khanRecords.Count;
            // ConversionProgressBar.Value = 0;

            if (string.IsNullOrWhiteSpace(RopotIdTextBox.Text))
            {
                throw new ArgumentNullException(nameof(RopotIdTextBox), "Je třeba vyplnit číselný identifikátor poznámkového bloku.");
            }

            string outputFolderPath = OutputFolderTextBox.Text.Trim();
            if (string.IsNullOrWhiteSpace(outputFolderPath))
            {
                string inputFolderPath = Path.GetDirectoryName(InputFileTextBox.Text.Trim()) ?? string.Empty;
                outputFolderPath = OutputFolderTextBox.Text = inputFolderPath;
            }

            var workerArgument = new OutputData(outputFolderPath,
                                                duoStudents,
                                                RopotIdTextBox.Text,
                                                int.Parse(PointsPerLessonTextBox.Text.Trim()),
                                                int.Parse(PointLimitTextBox.Text.Trim()),
                                                SortedCheckbox.IsChecked.GetValueOrDefault(false));
            worker.RunWorkerAsync(workerArgument); // Start the conversion
        }

        internal struct OutputData
        {
            public string folderPath;
            public List<DuolingoStudent> students;
            public string ropot;
            public int pointsPerLecture;
            public int pointsLimit;
            public bool sorted;

            /// <summary>Initializes a new instance of the <see cref="OutputData" /> class.</summary>
            public OutputData(string folderPath, List<DuolingoStudent> students, string ropot, int pointsPerLecture, int pointsLimit, bool sorted = false)
            {
                this.folderPath = folderPath;
                this.students = students;
                this.ropot = ropot;
                this.pointsPerLecture = pointsPerLecture;
                this.pointsLimit = pointsLimit;
                this.sorted = true;
            }
        }

        void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ConversionProgressBar.Value = e.ProgressPercentage;
        }
    }
}
